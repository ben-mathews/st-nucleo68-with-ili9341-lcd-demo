# ST Nucleo68 with ILI9341 LCD Demo

Demo of Controlling an ILI9341 TFT LCD from an ST P-NUCLEO-WB55 Board Using STM32CubeIDE.

See quick demo at: [https://youtu.be/h8rJvcM_c7A](https://youtu.be/h8rJvcM_c7A)

![Overview](doc/images/overview.png)

## Introduction

The purpose of this project is to demonstrate how to interface an Adafruit 1947 TFT LCD display with an ST Electronics P-NUCLEO-WB55 development board by building a project from scratch using the STM32CubeIDE.  Instructions for building the project and interfacing with the ILI8341 driver are included in this readme, and a sample project is included for reference purposes.

The Nucleo-68 board comes as part of the low cost [P-NUCLEO-WB55 kit](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html), and is built around an [STM32WB55RG](https://www.st.com/en/microcontrollers-microprocessors/stm32wb55rg.html) IC and provides additional circuitry and connectors to enable Bluetooth Low Energy (BLE), 802.15.4 mesh networking, and additional connectivity to facilitate debugging and development.  It can be interfaced with a PC via USB and programmed with ST's [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html) application, or other similar applications. 

## Hardware Set-Up

The pinouts for the Adafruit 1947 are described at [https://learn.adafruit.com/assets/27040]().  Additional information can be obtained by looking at the board layout and schematic files which are available at [https://github.com/adafruit/Adafruit-2.8-TFT-Shield-v2-PCB]().  These files can be viewed in the free version of [EAGLE CAD](https://www.autodesk.com/products/eagle/overview) or in [KiCad](https://www.kicad-pcb.org/).  A general description of Arduino Shield connector pinouts can be found at [https://learn.sparkfun.com/tutorials/arduino-shields/all](https://learn.sparkfun.com/tutorials/arduino-shields/all).  The pins on the Adafruit 1947's Shield connector and 2x3 header are not well marked on the board, so see these other resources to understand how to find them.  

The connector pinouts for the Nucelo-68 board are described in Figure 8 on page 16 of [https://www.st.com/resource/en/user_manual/dm00517423-bluetooth-low-energy-and-802154-nucleo-pack-based-on-stm32wb-series-microcontrollers-stmicroelectronics.pdf]().

The connections are summarized in the table and graphic below.  
* Power: The 5V and GND pins on the Nucelo-68 board are connected to the appropriate pins on the Arduino Shield connector on the Adafruit 1947.  
* IOREF: In order to tell the Adafruit 1947 to use 3.3V logic levels, the IOREF pin on the Shield connector must be connected to the 3.3V output of the Nucelo-68.  
* SPI: The SPI1 interface on the Nucleo-68's STM32WB55RG seem to use IC pins PA6, PA7, and PA1 for MISO, MOSI, and SCLK, respectively.  These come out of pins D12, D11, and A2 of the Nucleo-68. and go into the ICSP MISO, MOSI, and SCLK pins on the 2x3 header connector.  The Chip Select (CS) and Data/Commmand (DC) lines come out of STM32WB55RG IC pins PA9 and PC12, which come out of the Nucleo-68 on pins D9 and D8.  These go to the CS and DC pins on the Adafruit 1947's Arduino Shield connector. 
* Reset: We use PA5 on the STM32WB55RG IC which comes out of D13 of the Nucleu-68, and goes into the RST pin on the Shield connector.

| Nucleo68 Board    | Adafruit 1947 TFT LCD     |
| ----------------- | ------------------------- |
| 5 V               | 5V on Shield Connector    |
| GND               | GND on Shield Connector   |
| 3.3 V             | IOREF on Shield Connector |
| A2 (SPI1 CLK)     | SCLK on ICSP 2x3 Header   |
| D12 (SPI1 MISO)   | MISO on ICSP 2x3 Header   |
| D11 (SPI1 MOSI)   | MOSI on ICSP 2x3 Header   |
| D8                | DC on Shield Connector    |
| D9                | CS on Shield Connector    |
| D13               | RST on Shield Connector   |

Wiring Diagram:

![Connections Diagram](doc/images/connections.png)


## Development Environment Set-Up
This project was built using STM32CubeIDE version 1.3.1.  To install this software download the installer from [https://www.st.com/en/development-tools/stm32cubeide.html](https://www.st.com/en/development-tools/stm32cubeide.html) and run on your development machine.  After installation, do Help -> Check for Updates to get the latest minor version.

At various points in the development cycle you will be prompted to download firmware, libraries, etc.  This is normal.

## Project Set-Up
Steps for creating the base project.

1) Launch STM32CubeIDE

2) Select a new workspace or create a new one, and hit Launch.

    ![Screenshot](doc/images/project_setup_walkthrough/step_02.png)

3) Select "Start new STM32 project."

    ![Screenshot](doc/images/project_setup_walkthrough/step_03.png)

4) In the "Part Number Search" window enter "STM32WB55RG."  Click on the part and hit Next.

    ![Screenshot](doc/images/project_setup_walkthrough/step_04.png)

5) Enter a Project Name (e.g. "stm32wb55rg_ili9341_example_project"), keep the other defaults (C language, executable binary, STM32Cube project type), and hit Next.

    ![Screenshot](doc/images/project_setup_walkthrough/step_05.png)

6) Select the most up to date version of firmware.  I used V1.6.0 when creating this project.  Keep other defaults.

    ![Screenshot](doc/images/project_setup_walkthrough/step_06.png)

7) If asked to open the "STM32CubeMx" persepctive, select yes.

8) Using the Device Configuration Tool in the "STM32CubeMx" persepctive, configure the device:

    a) Enable debugging by going to System Core -> Sys and setting Debug to Serial Wire
    
    ![Screenshot](doc/images/project_setup_walkthrough/step_08a.png)
    
    b) Add a SPI bus by going to Connectivity -> SPI1 and changing mode to Transmit Only Master
    
    ![Screenshot](doc/images/project_setup_walkthrough/step_08b.png)
    
    c) Configure the SPI bus settings by selecting Parameter Settings and changing the Data Size from 4 bits to 8 bits
    
    ![Screenshot](doc/images/project_setup_walkthrough/step_08c.png)
        
    d) Configure the GPIO pins of SPI bus by selecting GPIO Settings, selecting each pin, and scrolling to the bottom of the configuringation window and setting "Maximum Output Speed" to "Very High".  The IDE unintentionally hides this setting by making you scroll to the bottom of the window to see it.
    
    ![Screenshot](doc/images/project_setup_walkthrough/step_08d.png)
    
    e) Configure pinx PA9, PC12, and PA5 as LCD_CS, LCD_DC, and LCD_RST pins, respectively.  To do this, for each pin left click and select GPIO_Output, then right click and select "Enter User Label".  The label must be set exactly to "LCD_CS", "LCS_DC", or "LCD_RST".
    
    ![Screenshot](doc/images/project_setup_walkthrough/step_08e.png)    
    
    f) (I believe this step is optional) For max performance, update the clock configuration.    Set MSI RC to 32000 KHz, set the PLLM to "/2", change the System Clock Mux to PLLCLK, and change CPU2 HPRE to "/2"
    
    ![Screenshot](doc/images/project_setup_walkthrough/step_08f.png)  
    
9) To generate code, do File -> Save and Select Yes when asked if you want to generate Code
    
10) With the code generated, first make sure the project can build.  Run Project -> Build All and make sure the build finishes with no errors or warnings.

    ![Screenshot](doc/images/project_setup_walkthrough/step_09.png)

11) Now make sure the project can deploy and run on your Nucleo-68 board.  Do Run -> Run, and select the defaults in the Configuration dialog, and hit Ok.

    ![Screenshot](doc/images/project_setup_walkthrough/step_10.png)

12) The activity light on your Nucleo-68 should flash red and green, and no errors should be shown in the console. 

    ![Screenshot](doc/images/project_setup_walkthrough/step_10.png)

Completing this process through step 12 demonstrates that you've been able to create the project, configure the device, and build and run boilerpate code.

## Adding the ILI9341 Driver to the Project and Running Demo
Next the ILI9341 driver code must be added to the project.

1) Obtain the driver code.  This repo contains code that is known to compile cleanly in HAL projects with STM32CubeIDE 1.3.1.  Other options include getting it from ... or from ... .

2) (Optional) To make things a little easier, the code can be dropped into the project tree structure.  I don't believe this is necessary, but it helps keep things together.  In this case, I place it in the Drivers folder.

    ![Screenshot](doc/images/adding_driver_walkthrough/step_01.png)
    
3) Now we tell Eclipse to look for the driver code.  In the Project Explorer (typically on the left), right click on the project and select Properties.  Under C/C++ General, select "Paths and Symbols".  Add an Include directory that points back to the location from step 1 above.  If using the location from step 2, the "${ProjDirPath}" variable can be used to reference the project directory.      

    ![Screenshot](doc/images/adding_driver_walkthrough/step_02.png)

4) The .h file can now be included.  Open 'Core/Src/main.c' and find the `/* USER CODE BEGIN Includes */` line near the top (on line 25 in my code).  Immediately after this, add lines to include "ILI9341_STM32_Driver.h" and "ILI9341_GFX.h".  This has to be included between USER CODE BEGIN and END in order to prevent STM32CubeIDE from overwritting it when regenerating the code in later steps.  Optionally also add "stdlib.h" and "stdio.h" to avoid compiler warnings if using the demo code provided below.

    ![Screenshot](doc/images/adding_driver_walkthrough/step_03.png)
    
5) The ILI9341 driver depends on a `#define` to set `HSPI_INSTANCE` to the name of the SPI interface handle.  Assuming SPI1 was used in the previous section, this will be `hspi1`.  Open Core/Inc/main.h, and after the `/* USER CODE BEGIN Private defines */` line add, `#define HSPI_INSTANCE hspi1`

    ![Screenshot](doc/images/adding_driver_walkthrough/step_04.png)

6) We need to tell the project to call the ILI9341 initialization function.  In the `main()` function look for the place where the GPIO and SPI interfaces are initialized, and immediately following that should be the `/* USER CODE BEGIN 2 */` line.  After this line insert the call to `ILI9341_Init()`.

    ![Screenshot](doc/images/adding_driver_walkthrough/step_05.png)
    
7) Now demo code can be added to exercise the LCD.  This should nominally go inside the `while(1)` loop in `main()`, immediately after `/* USER CODE BEGIN 3 */`.  Some [great demo code](https://github.com/elmot/STM32-ILI9341/blob/master/stm32l476-ili9341-example/Core/Src/demos.c) is offered by [elmot](https://github.com/elmot/), from which I've stolen and modified a few snippets below:

    ![Screenshot](doc/images/adding_driver_walkthrough/step_06.png)

```c
ILI9341_Fill_Screen(WHITE);
ILI9341_Set_Rotation(SCREEN_HORIZONTAL_2);
ILI9341_Draw_Text("Randomly placed and sized", 10, 10, BLACK, 1, WHITE);
ILI9341_Draw_Text("objects", 10, 20, BLACK, 1, WHITE);
HAL_Delay(2000);
ILI9341_Fill_Screen(WHITE);

for (uint32_t i = 0; i < 100000; i++) {
  uint32_t random_num = 0;
  uint16_t xr = 0;
  uint16_t yr = 0;
  uint16_t radiusr = 0;
  uint16_t colourr = 0;

  random_num = random();
  xr = random_num;
  random_num = random();
  yr = random_num;
  random_num = random();
  radiusr = random_num;
  random_num = random();
  colourr = random_num;

  xr &= 0x01FFu;
  yr &= 0x01FFu;
  radiusr &= 0x001Fu;
  ILI9341_Draw_Filled_Circle(xr, yr, radiusr / 2, colourr);

  xr = random() % ILI9341_SCREEN_WIDTH;
  yr = random() % ILI9341_SCREEN_HEIGHT;
  radiusr = random() % ILI9341_SCREEN_HEIGHT /10;
  colourr = random();
  ILI9341_Draw_Rectangle(xr, yr, radiusr, radiusr, colourr);

  xr = random();
  yr = random();
  radiusr = random();
  colourr = random();

  xr &= 0x01FFu;
  yr &= 0x01FFu;
  radiusr &= 0x001Fu;

  ILI9341_Draw_Horizontal_Line(xr, yr, radiusr, colourr);
  ILI9341_Draw_Vertical_Line(xr, yr, radiusr, colourr);
}
char buf[100];
snprintf(buf, 99, "Done!");
ILI9341_Draw_Text(buf, 2, 2, YELLOW, 3, DARKCYAN);
HAL_Delay(10000);
HAL_Delay(1000);
```


## Notes

Convert images to raw rgb565 format with `ffmpeg -i ../0001.gif -pix_fmt rgb565 output_%05d.raw`